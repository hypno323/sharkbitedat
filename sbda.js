//Game Variables

var sharkX = 160;
var sharkY = 40;
var guessX = 0;
var guessY = 0; 
var gameWon = false;

//Game Objects

var player = document.querySelector("#player");
var shark = document.querySelector("#shark");
var missile = document.querySelector("#missile");

//The Input and Output fields

var inputX = document.querySelector("#inputX");
var inputY = document.querySelector("#inputY");
var output = document.querySelector("#output");

//The Button

var fireButton = document.querySelector("#fireButton");
fireButton.style.cursor = "pointer";
fireButton.addEventListener("click", clickHandler, false);

//Functions

function clickHandler()
{
    validateInput();
}



function render()
{
    //Position Shark
    shark.style.left = sharkX + "px";
    shark.style.top = sharkY + "px";
    //Position Player
    player.style.left = guessX + "px";
    //Position Missile 
    missile.style.left = guessX + "px";
    missile.style.top = guessY + "px";
}

function validateInput() 
{ 
 guessX = parseInt(inputX.value); 
 guessY = parseInt(inputY.value);
 if(isNaN(guessX) || isNaN(guessY)) 
 { 
 output.innerHTML = "Please enter a number."; 
 } 
 else if(guessX > 800 || guessY > 600) 
 { 
 output.innerHTML = "Please enter a number less than for X less then 800 and for Y less then 600."; 
 } 
 else 
 { 
 playGame(); 
 } 
}

//Start Game

function playGame()
    {

        
        //Find out whether the player's x and y  guesses are inside
        //the sharks area

        if (guessX >= sharkX && guessX <= sharkX + 96) {
        //Yes, it's within the X range, so now let's
        //check the Y range

            if (guessY >= sharkY && guessY <= sharkY + 96) {
                 //It's in both the X and Y range, so it's a hit!
                output.innerHTML = "It's a Hit!";
                gameWon = true;
            }
        } else {
            output.innerHTML = "Miss!";
            }
            if(!gameWon) {
                sharkX = Math.floor(Math.random() * 705)
                sharkY += 50
                }
            
        
            render();
            console.log("X: " + sharkX);
            console.log("Y: " + sharkY);

            
    }

    



